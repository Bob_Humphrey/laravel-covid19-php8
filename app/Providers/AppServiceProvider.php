<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\StateHistoricalData;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    session(['earliest_date' => '2020-04-01']);
  }
}
