<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdatePerCapitaJob;

class UpdatePerCapita extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_per_capita';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update historical per capita amounts';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdatePerCapitaJob::dispatch();
  }
}
