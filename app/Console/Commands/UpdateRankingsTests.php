<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateRankingsJob;

class UpdateRankingsTests extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_rankings_tests';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update rankings - tests';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateRankingsJob::dispatch('total_tests');
    UpdateRankingsJob::dispatch('total_tests_per_capita');
    UpdateRankingsJob::dispatch('seven_day_tests');
    UpdateRankingsJob::dispatch('seven_day_tests_increase');
    UpdateRankingsJob::dispatch('seven_day_tests_average');
    UpdateRankingsJob::dispatch('fourteen_day_tests_change');
    UpdateRankingsJob::dispatch('doubling_tests');
    UpdateRankingsJob::dispatch('daily_tests');
    UpdateRankingsJob::dispatch('tests_percent_positive');
  }
}
