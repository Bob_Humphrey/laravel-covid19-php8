<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Helpers\Misc;


class Director extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:director';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Run each of the daily update jobs, one at a time';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $jobStart = Carbon::now();
    $updatedAt = $jobStart->toDateTimeString();
    Log::info('Director started at ' . $jobStart->toDayDateTimeString());
    $this->info('Started ' . $jobStart->toDayDateTimeString());
    // $earliestDate = '2020-12-17';
    // $latestDate = '2020-12-19';
    $latestDate = Carbon::now()->addDay()->format('Y-m-d');
    $earliestDate = Carbon::now()->subWeeks(4)->format('Y-m-d');
    $this->info('Earliest date ' . $earliestDate);
    $this->info('Latest date ' . $latestDate);

    // $this->call('migrate:fresh');

    DB::table('job_dates')
      ->updateOrInsert(
        ['type' => 'earliest date'],
        ['date' => $earliestDate, 'created_at' => $updatedAt, 'updated_at' => $updatedAt]
      );

    DB::table('job_dates')
      ->updateOrInsert(
        ['type' => 'latest date'],
        ['date' => $latestDate, 'created_at' => $updatedAt, 'updated_at' => $updatedAt]
      );

    Misc::jobStart('Director');

    // $this->call('app:load_state_table');
    // $this->info('Finished app:load_state_table');

    $this->call('app:load_api_data');
    $this->info('Finished app:load_api_data');

    $this->call('app:update_averages');
    $this->info('Finished app:update_averages');

    $this->call('app:update_moving_averages');
    $this->info('Finished app:update_moving_averages');

    $this->call('app:update_per_capita');
    $this->info('Finished app:update_per_capita');

    $this->call('app:update_categories');
    $this->info('Finished app:update_categories');

    $this->call('app:update_trend_change');
    $this->info('Finished app:update_trend_change');

    $this->call("app:update_rankings_deaths");
    $this->info('Finished app:update_rankings_deaths');

    $this->call("app:update_rankings_cases");
    $this->info('Finished app:update_rankings_cases');

    $this->call("app:update_rankings_tests");
    $this->info('Finished app:update_rankings_tests');

    $jobEnd = Carbon::now();
    $minutes = $jobEnd->diffInMinutes($jobStart);
    $this->info('Finished ' . $jobEnd->toDayDateTimeString());

    Misc::jobEnd('Director', $minutes);
  }
}
