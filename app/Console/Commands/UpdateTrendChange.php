<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateTrendChangeJob;

class UpdateTrendChange extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_trend_change';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update states with better/worse 14 day trends';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateTrendChangeJob::dispatch();
  }
}
