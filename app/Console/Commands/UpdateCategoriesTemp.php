<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\UpdateCategoriesJobTemp;

class UpdateCategoriesTemp extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'app:update_categories_temp';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Update daily measure categories';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    UpdateCategoriesJobTemp::dispatch();
  }
}
