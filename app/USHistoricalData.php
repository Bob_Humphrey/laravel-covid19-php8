<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class USHistoricalData extends Model
{
  protected $table = 'us_historical_data';
}
