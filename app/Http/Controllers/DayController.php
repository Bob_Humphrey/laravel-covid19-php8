<?php

namespace App\Http\Controllers;

use App\StateHistoricalData;
use App\USHistoricalData;
use App\Helpers\Scales;
use App\Helpers\Measures;
use stdClass;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class DayController extends Controller
{

  public function index($measure, $date = null)
  {
    if ($date === null) {
      $date = session('last_used_date');
    }
    if ($date === null) {
      // $date = Carbon::now()->subDay()->format('Y-m-d');
      $date = '2021-03-07';
    }
    $requestedDate = Carbon::createFromFormat('Y-m-d', $date);
    $lastDateMeasured = Carbon::createFromFormat('Y-m-d', '2021-03-07');
    if ($requestedDate->greaterThan($lastDateMeasured)) {
      $date = '2021-03-07';
    }

    session(['last_used_date' => $date]);

    $category = "category_" . $measure;
    $rank = "rank_" . $measure;
    $pageTitle = Measures::getHeading($measure);
    $scaleProperty = $measure . '_scale';
    $objScales = new Scales($scaleProperty);
    $scale = $objScales->getScale();
    $originalStateData = StateHistoricalData::select('state', $measure, $category, $rank)
      ->where('day', $date)
      ->orderBy($rank, 'ASC')
      ->orderBy('state', 'ASC')
      ->get();
    $stateData = [];
    foreach ($originalStateData as $o) {
      $item = new stdClass();
      $item->state = $o->state;
      $item->measure = $o[$measure];
      $item->category = $o[$category];
      $item->rank = $o[$rank];
      $stateData[] = $item;
    }
    $usData = USHistoricalData::select($measure)->where('day', $date)->first();
    $usData->measure = $usData[$measure];

    return view('day', [
      'pageTitle' => $pageTitle,
      'date' => $date,
      'stateData' => $stateData,
      'usData' => $usData,
      'scale' => $scale,
      'path' => 'day/' . $measure
    ]);
  }

  public function home($date = null)
  {
    if ($date === null) {
      $date = session('last_used_date');
    }
    if ($date === null) {
      // $date = Carbon::now()->subDay()->format('Y-m-d');
      $date = '2021-03-07';
    }
    $requestedDate = Carbon::createFromFormat('Y-m-d', $date);
    $lastDateMeasured = Carbon::createFromFormat('Y-m-d', '2021-03-07');
    if ($requestedDate->greaterThan($lastDateMeasured)) {
      $date = '2021-03-07';
    }
    session(['last_used_date' => $date]);

    // US Total Deaths and Cases

    $usData = USHistoricalData::where('day', $date)
      ->first();

    // Seven Day Cases Daily Average

    $sevenDayAvgCasesData = [];

    $d = USHistoricalData::select(
      'day',
      'seven_day_cases_average',
    )
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->seven_day_cases_average;
      $sevenDayAvgCasesData[] = $item;
    }

    // Seven Day Average Cases Per Million

    $measure = 'average_cases_per_capita';
    $category = "category_" . $measure;
    $rank = "rank_" . $measure;
    $scaleProperty = $measure . '_scale';
    $objScales = new Scales($scaleProperty);
    $scaleCasesPerCapita = $objScales->getScale();
    $originalStateData = StateHistoricalData::select('state', $measure, $category, $rank)
      ->where('day', $date)
      ->orderBy($rank, 'ASC')
      ->orderBy('state', 'ASC')
      ->get();
    $stateDataCasesPerCapita = [];
    foreach ($originalStateData as $o) {
      $item = new stdClass();
      $item->state = $o->state;
      $item->measure = $o[$measure];
      $item->category = $o[$category];
      $item->rank = $o[$rank];
      $stateDataCasesPerCapita[] = $item;
    }

    // Fourteen Day Cases Percent Change

    $measure = 'fourteen_day_cases_change';
    $category = "category_" . $measure;
    $rank = "rank_" . $measure;
    $scaleProperty = $measure . '_scale';
    $objScales = new Scales($scaleProperty);
    $cases14Scale = $objScales->getScale();
    $originalStateData = StateHistoricalData::select('state', $measure, $category, $rank)
      ->where('day', $date)
      ->orderBy($rank, 'ASC')
      ->orderBy('state', 'ASC')
      ->get();
    $stateData = [];
    foreach ($originalStateData as $o) {
      $item = new stdClass();
      $item->state = $o->state;
      $item->measure = $o[$measure];
      $item->category = $o[$category];
      $item->rank = $o[$rank];
      $stateData[] = $item;
    }
    $cases14Data = $stateData;

    // Chart data

    $d = USHistoricalData::select(
      'day',
      'fourteen_day_cases_change',
      'fourteen_day_cases_better',
      'fourteen_day_cases_same',
      'fourteen_day_cases_worse'
    )
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $casesBetterItem = new stdClass();
      $casesBetterItem->day = $o->day;
      $casesBetterItem->measure = $o->fourteen_day_cases_better;
      $casesBetter[] = $casesBetterItem;
      $casesSameItem = new stdClass();
      $casesSameItem->day = $o->day;
      $casesSameItem->measure = $o->fourteen_day_cases_same;
      $casesSame[] = $casesSameItem;
      $casesWorseItem = new stdClass();
      $casesWorseItem->day = $o->day;
      $casesWorseItem->measure = $o->fourteen_day_cases_worse;
      $casesWorse[] = $casesWorseItem;
    }

    // State table

    $measure = "fourteen_day_cases_change";
    $category = "category_" . $measure;
    $rank = "rank_" . $measure;
    $pageTitle = Measures::getHeading($measure);
    $scaleProperty = $measure . '_scale';
    $objScales = new Scales($scaleProperty);
    $scale = $objScales->getScale();
    $originalStateData = StateHistoricalData::select('state', $measure, $category, $rank)
      ->where('day', $date)
      ->orderBy($rank, 'ASC')
      ->orderBy('state', 'ASC')
      ->get();
    $stateData = [];
    foreach ($originalStateData as $o) {
      $item = new stdClass();
      $item->state = $o->state;
      $item->measure = $o[$measure];
      $item->category = $o[$category];
      $item->rank = $o[$rank];
      $stateData[] = $item;
    }
    $usTableData = USHistoricalData::select($measure)->where('day', $date)->first();

    Log::info($date);
    Log::info($measure);
    Log::info($usTableData);
    $usTableData->measure = $usData[$measure];

    return view('home', [
      'date' => $date,
      'path' => 'home',
      'usData' => $usData,
      'sevenDayAvgCasesData' => $sevenDayAvgCasesData,
      'casesWorse' => $casesWorse,
      'casesSame' => $casesSame,
      'casesBetter' => $casesBetter,
      'scaleCasesPerCapita' => $scaleCasesPerCapita,
      'stateDataCasesPerCapita' => $stateDataCasesPerCapita,
      'stateData' => $stateData,
      'usTableData' => $usTableData,
      'pathTable' => 'day/' . $measure
    ]);
  }
}
