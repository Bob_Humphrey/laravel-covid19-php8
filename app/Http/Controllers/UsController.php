<?php

namespace App\Http\Controllers;

use App\USHistoricalData;
use App\Helpers\Us;
use Carbon\Carbon;
use stdClass;
use Illuminate\Support\Facades\Log;

class UsController extends Controller
{
  public function index($date = null)
  {
    if ($date === null) {
      $date = session('last_used_date');
    }
    if ($date === null) {
      $date = Carbon::now()->subDay()->format('Y-m-d');
    }
    session(['last_used_date' => $date]);
    $data = [];

    $d = USHistoricalData::select(
      'day',
      'seven_day_cases_average',
    )
      ->orderBy('day', 'DESC')
      ->get();

    foreach ($d as $o) {
      $item = new stdClass();
      $item->day = $o->day;
      $item->measure = $o->seven_day_cases_average;
      $data[] = $item;
    }

    $record = USHistoricalData::where('day', $date)
      ->first();

    $population = new stdClass();
    $population->population = Us::$population;
    $population->population_rank = "";

    return view('us', [
      'pageTitle' => "United States",
      'record' => $record,
      'date' => $date,
      'data' => $data,
      'population' => $population,
      'path' => 'us'
    ]);
  }
}
