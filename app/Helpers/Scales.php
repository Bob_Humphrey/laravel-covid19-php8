<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Log;

class Scales
{
  private $ascendingScales = [
    "total_deaths_scale" => 400,
    "total_deaths_per_capita_scale" => 40,
    "seven_day_deaths_scale" => 60,
    "seven_day_deaths_increase_scale" => 4,
    "seven_day_deaths_average_scale" => 9,
    "daily_deaths_scale" => 10,
    "total_cases_scale" => 6000,
    "total_cases_per_capita_scale" => 1000,
    "seven_day_cases_scale" => 1000,
    "seven_day_cases_increase_scale" => 4,
    "seven_day_cases_average_scale" => 142,
    "daily_cases_scale" => 125,
    "tests_percent_positive_scale" => 2.5,
    "average_cases_per_capita_scale" => 125,
  ];

  private $descendingScales = [
    "total_tests_scale" => 100000,
    "total_tests_per_capita_scale" => 10000,
    "seven_day_tests_scale" => 17500,
    "seven_day_tests_increase_scale" => 5,
    "seven_day_tests_average_scale" => 2500,
    "daily_tests_scale" => 2000
  ];

  private $doublingScales = [
    "doubling_deaths_scale" => 5,
    "doubling_cases_scale" => 5,
    "doubling_tests_scale" => 5,
  ];

  private $fourteenDayTrendScales = [
    "fourteen_day_deaths_change_scale" => 0,
    "fourteen_day_cases_change_scale" => 0,
    "fourteen_day_tests_change_scale" => 0,
  ];

  private $colors = [
    "#7CB342", // light green
    "#C0CA33", // lime
    "#FDD835", // yellow
    "#FFB300", // amber
    "#FB8C00", // orange
    "#F4511E", // deep orange
    "#e53935", // red
    "#b71c1c", // dark red
    "#8a1515"  // darkest red
  ];

  private $scale = [];
  private $scaleGroup;

  public function __construct($scaleType = null)
  {

    if ($scaleType === null) {
      return;
    }
    $scale = [];
    $scaleGroup = "";
    $scaleAmount = 0;
    if (array_key_exists($scaleType, $this->ascendingScales)) {
      $scaleAmount = $this->ascendingScales[$scaleType];
      $scaleGroup = "ascending";
    } elseif (array_key_exists($scaleType, $this->descendingScales)) {
      $scaleAmount = $this->descendingScales[$scaleType];
      $scaleGroup = "descending";
    } elseif (array_key_exists($scaleType, $this->doublingScales)) {
      $scaleAmount = $this->doublingScales[$scaleType];
      $scaleGroup = "doubling";
    } elseif (array_key_exists($scaleType, $this->fourteenDayTrendScales)) {
      $scaleGroup = "fourteenDay";
    }
    if ($scaleGroup === "ascending") {
      for ($i = 0; $i < 9; $i++) {
        $scale[] = $i * $scaleAmount;
      }
    } elseif ($scaleGroup === "descending") {
      for ($i = 8; $i >= 0; $i--) {
        $scale[] = $i * $scaleAmount;
      }
    } elseif ($scaleGroup === "doubling") {
      for ($i = 8; $i >= 0; $i--) {
        $scale[] = ($i + 1) * $scaleAmount;
      }
    } elseif ($scaleGroup === "fourteenDay") {
      $scale = [-5, -2.5, 0, 2.5, 5, 7.5, 10, 12.5, 15];
    }
    $this->scale = $scale;
    $this->scaleGroup = $scaleGroup;
  }

  private function getCategoryAscending($amount)
  {
    for ($i = 0; $i < count($this->scale) - 1; $i++) {
      if ($amount < $this->scale[$i + 1]) {
        return $i;
      }
    }
    return 8;
  }

  private function getCategoryDescending($amount)
  {
    for ($i = 0; $i < count($this->scale); $i++) {
      if ($amount >= $this->scale[$i]) {
        return $i;
      }
    }
    return 0;
  }

  private function getCategoryDoubling($amount)
  {

    for ($i = 0; $i < count($this->scale); $i++) {
      if ($amount >= $this->scale[$i]) {
        return $i;
      }
    }
    return 0;
  }

  private function getCategoryFourteenDay($amount)
  {
    for ($i = 0; $i < count($this->scale) - 1; $i++) {
      if ($amount < $this->scale[$i + 1]) {
        return $i;
      }
    }
    return 8;
  }

  public function getColor($code)
  {
    return $this->colors[$code];
  }

  public function getCategory($amount)
  {
    $category = 0;
    if ($this->scaleGroup === "ascending") {
      $category = $this->getCategoryAscending($amount);
      return $category;
    } elseif ($this->scaleGroup === "descending") {
      $category = $this->getCategoryDescending($amount);
      return $category;
    } elseif ($this->scaleGroup === "doubling") {
      $category = $this->getCategoryDoubling($amount);
      return $category;
    } elseif ($this->scaleGroup === "fourteenDay") {
      $category = $this->getCategoryFourteenDay($amount);
      return $category;
    }
  }

  public function getScale()
  {
    return $this->scale;
  }
}
