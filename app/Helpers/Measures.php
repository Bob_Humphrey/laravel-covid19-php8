<?php

namespace App\Helpers;

class Measures
{
  public static $measures = [
    'total_deaths' => 'Total Deaths',
    'total_deaths_per_capita' => 'Total Deaths per Million',
    //    'seven_day_deaths' => 'Seven Day Deaths',
    'seven_day_deaths_average' => 'Seven Day Deaths Daily Average',
    //    'seven_day_deaths_increase' => 'Seven Day Deaths Pct Increase',
    'fourteen_day_deaths_change' => 'Fourteen Day Deaths Pct Change',
    //    'doubling_deaths' => 'Days for Deaths to Double',
    'daily_deaths' => 'Daily Deaths',
    'total_cases' => 'Total Cases',
    'total_cases_per_capita' => 'Total Cases per Million',
    // 'seven_day_cases' => 'Seven Day Cases',
    'seven_day_cases_average' => 'Seven Day Cases Daily Average',
    'average_cases_per_capita' => 'Seven Day Avg Cases per Million',
    // 'seven_day_cases_increase' => 'Seven Day Cases Pct Increase',
    'fourteen_day_cases_change' => 'Fourteen Day Cases Pct Change',
    // 'doubling_cases' => 'Days for Cases to Double',
    'daily_cases' => 'Daily Cases',
    'total_tests' => 'Total Tests',
    // 'total_tests_per_capita' => 'Total Tests per Million',
    // 'seven_day_tests' => 'Seven Day Tests',
    'seven_day_tests_average' => 'Seven Day Tests Daily Average',
    // 'daily_tests' => 'Daily Tests',
    'tests_percent_positive' => 'Positive Tests Percent',
  ];

  public static $definitions = [
    'total_deaths' => 'The total number of persons who have died, through the selected date.',
    'total_deaths_per_capita' => 'The number of persons who have died, through the selected date, relative to the population size.  (total deaths * 1000000 / population size)',
    'seven_day_deaths' => 'The number of persons who have died during the previous seven days, up to and including the selected date.',
    'seven_day_deaths_average' => 'The daily average of the number of persons who have died during the previous seven days, up to and including the selected date. (last seven day deaths / 7)',
    'seven_day_deaths_increase' => 'The percentage increase in the number of deaths over the past week. ((seven day deaths * 100) / one week ago total deaths)',
    'fourteen_day_deaths_change' => 'The percentage change in the average seven day death rate, as compared to fourteen days earlier. (((current average seven day deaths - average seven day deaths from 14 days earlier) / average seven day deaths from 14 days earlier) * 100)',
    'doubling_deaths' => 'The number of days it will take for the total number of deaths to double, based upon the current rate of increase.  (7 * log(2)) / (log(current total deaths / week ago total deaths))',
    'daily_deaths' => 'The number of persons who have died on the selected date.',
    'total_cases' => 'The total number of persons who have tested positive, through the selected date.',
    'total_cases_per_capita' => 'The number of persons who have tested positive, through the selected date, relative to the population size.  (total cases * 1000000 / population size)',
    'seven_day_cases' => 'The number of persons who have tested positive in the previous seven days, up to and including the selected date.',
    'seven_day_cases_average' => 'The daily average of the number of persons who have tested positive in the previous seven days, up to and including the selected date. (last seven day cases / 7)',
    'average_cases_per_capita' => 'The daily average of the number of persons who have tested positive in the previous seven days, up to and including the selected date, relative to the population size.  (7 day average cases * 1000000 / population size)',
    'seven_day_cases_increase' => 'The percentage increase in the number of persons who have tested positive over the past week. ((seven day cases * 100) / one week ago total cases)',
    'fourteen_day_cases_change' => 'The percentage change in the average seven day rate of persons testing positive, as compared to fourteen days earlier. (((current average seven day cases - average seven day cases from 14 days earlier) / average seven day cases from 14 days earlier) * 100)',
    'doubling_cases' =>
    'The number of days it will take for the total number of persons testing positive to double, based upon the current rate of increase.  (7 * log(2)) / (log(current total cases / week ago total cases))',
    'daily_cases' => 'The number of persons who have tested positive on the selected date.',
    'total_tests' => 'The number of tests conducted in the previous seven days, up to and including the selected date.',
    'total_tests_per_capita' => 'The number of tests conducted, through the selected date, relative to the population size.  (total tests * 1000000 / population size)',
    'seven_day_tests' => 'The number of tests conducted in the previous seven days, up to and including the selected date.',
    'seven_day_tests_average' => 'The daily average of the number of tests conducted in the previous seven days, up to and including the selected date. (last seven day tests / 7)',
    'daily_tests' => 'The number of tests conducted on the selected date.',
    'tests_percent_positive' => 'The number of infected persons found, as a percentage of the total number of tests conducted over the previous seven days. Lower percentages include improving test coverage. ((seven day average cases * 100) / seven day average number of tests conducted)',
  ];

  public static $startChartApril15 = [
    'doubling_deaths', 'doubling_cases', 'doubling_tests',
    'seven_day_deaths_increase', 'seven_day_cases_increase', 'seven_day_cases_increase',
    'tests_percent_positive',
  ];

  public static $startChartMay1 = [
    'fourteen_day_deaths_change', 'fourteen_day_cases_change',
  ];

  public static $percentage = ['seven_day_deaths_increase', 'fourteen_day_deaths_change', 'seven_day_cases_increase', 'fourteen_day_cases_change', 'tests_percent_positive'];

  public static function getHeading($page)
  {
    return self::$measures[$page];
  }
}
