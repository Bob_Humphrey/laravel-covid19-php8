<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\Measures;
use Carbon\Carbon;
use stdClass;

class UsDataTable extends Component
{
  public $dataTable;
  public $date;
  public $population;
  public $formattedDate;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($record, $date, $population)
  {
    $dataTable = [];
    foreach (Measures::$measures as $key => $value) {
      $measureAmount = $record[$key];
      $item = new stdClass();
      $item->name = $value;
      $item->nameShort = substr($value, 0, 20);
      $item->measure = $key;
      $item->measureAmount = $measureAmount;
      $item->historyUrl = url('us/history/' . $key);
      $item->url = url('day/' . $key . '/' . $date);
      $item->formattedMeasure = number_format($measureAmount);
      if (in_array($key, Measures::$percentage)) {
        $item->formattedMeasure .= '%';
      }
      $dataTable[] = $item;
    }
    $population->formattedPopulation = number_format($population->population);
    $this->formattedDate = Carbon::createFromFormat('Y-m-d', $date)
      ->format('F j, Y');
    $this->dataTable = $dataTable;
    $this->date = $date;
    $this->population = $population;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.us-data-table');
  }
}
