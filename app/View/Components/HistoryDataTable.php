<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Helpers\Measures;
use App\Helpers\Scales;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class HistoryDataTable extends Component
{
  public $dataTable;
  public $measure;
  public $measureName;
  public $state;
  public $stateName;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($data, $measure, $state, $stateName = null)
  {
    //Log::info("HistoryDataTable.php=$stateName");
    $scales = new Scales();
    $dataTable = [];
    foreach ($data as $item) {
      $item->color = $scales->getColor($item->category);
      $item->style = "background-color:" . $item->color . ";";
      $item->date = Carbon::createFromFormat('Y-m-d', $item->day);
      $item->formattedDate = $item->date->format('F j, Y');
      $item->formattedRank = $item->rank === 0 ? "" : $item->rank;
      $item->url = url('day/' . $measure . '/' . $item->day);
      $item->stateUrl = url('state/' . $state . '/' . $item->day);
      $item->formattedMeasure = $item->measure == 9999999 ? "" : number_format($item->measure);
      if (in_array($measure, Measures::$percentage)) {
        $item->formattedMeasure .= '%';
      }
      $dataTable[] = $item;
    }
    $this->dataTable = $dataTable;
    $this->measure = $measure;
    $this->measureName = Measures::getHeading($measure);
    $this->state = $state;
    $this->stateName = $stateName;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\View\View|string
   */
  public function render()
  {
    return view('components.history-data-table');
  }
}
