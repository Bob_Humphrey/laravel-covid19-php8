<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use App\Helpers\Misc;
use App\StateData;


class LoadStateTableJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    Misc::jobStart('LoadStateTableJob');
    $populationFile = Storage::get('population.csv');
    $records = explode("\r\n", $populationFile);
    foreach ($records as $record) {
      $fields = explode(",", $record);
      $s = new StateData();
      $s->population = $fields[0];
      $s->abbreviation = $fields[1];
      $s->name = $fields[2];
      $s->population_rank = intval($fields[3]);
      $s->save();
    }
    Misc::jobEnd('LoadStateTableJob');
  }
}
