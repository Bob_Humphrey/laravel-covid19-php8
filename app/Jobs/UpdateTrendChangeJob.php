<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\StateHistoricalData;
use App\USHistoricalData;
use App\Helpers\Scales;
use App\Helpers\Measures;
use App\Helpers\Misc;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class UpdateTrendChangeJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdateTrendChangeJob');

    // Reinitialize counts for each of the days being processed. 
    $reinitDate = Carbon::createFromFormat('Y-m-d', '2020-03-14');
    $quitReinitDate = Carbon::tomorrow();

    while ($reinitDate->lessThan($quitReinitDate)) {

      if ($reinitDate->lessThan($job['earliestDate'])) {
        $reinitDate->addDay();
        continue;
      }
      if ($reinitDate->greaterThan($job['latestDate'])) {
        $reinitDate->addDay();
        continue;
      }

      $day = $reinitDate->format('Y-m-d');
      $us = USHistoricalData::where('day', $day)->first();

      if (!is_null($us)) {
        $us->fourteen_day_deaths_better = 0;
        $us->fourteen_day_deaths_same = 0;
        $us->fourteen_day_deaths_worse = 0;
        $us->fourteen_day_cases_better = 0;
        $us->fourteen_day_cases_same = 0;
        $us->fourteen_day_cases_worse = 0;
        $us->save();
      }

      $reinitDate->addDay();
    }

    StateHistoricalData::chunk(50, function ($records) use ($job) {

      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $us = USHistoricalData::where('day', $r->day);
        $category = $r->category_fourteen_day_deaths_change;

        if ($category === 0) {
          $us->increment('fourteen_day_deaths_better');
        } else if ($category >= 1 && $category <= 3) {
          $us->increment('fourteen_day_deaths_same');
        } else if ($category > 3) {
          $us->increment('fourteen_day_deaths_worse');
        }

        $category = $r->category_fourteen_day_cases_change;

        if ($category === 0) {
          $us->increment('fourteen_day_cases_better');
        } else if ($category >= 1 && $category <= 3) {
          $us->increment('fourteen_day_cases_same');
        } else if ($category > 3) {
          $us->increment('fourteen_day_cases_worse');
        }
      }
    });
  }
}
