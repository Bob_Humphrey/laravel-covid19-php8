<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\StateHistoricalData;
use App\USHistoricalData;
use App\Helpers\Misc;

class UpdateAveragesJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdateAveragesJob');

    StateHistoricalData::chunk(200, function ($records) use ($job) {
      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $weekAgo = Misc::getWeekAgo($r->day);
        $state = $r->state;

        $r7 = StateHistoricalData::where('state', $state)->where('day', $weekAgo)->first();
        if ($r7) {

          // Deaths
          $r->seven_day_deaths_average = ($r->total_deaths - $r7->total_deaths) / 7;
          $r->seven_day_deaths = $r->total_deaths - $r7->total_deaths;
          $r->seven_day_deaths_increase =  $r7->total_deaths === 0 ?
            0 : ($r->total_deaths - $r7->total_deaths) * 100 / $r7->total_deaths;
          if ($r7->total_deaths == 0) {
            $r->doubling_deaths = 9999999;
          } else if ((log($r->total_deaths / $r7->total_deaths)) == 0) {
            $r->doubling_deaths = 9999999;
          } else
            $r->doubling_deaths = (7 * log(2)) / (log($r->total_deaths / $r7->total_deaths));

          // Cases
          $r->seven_day_cases_average = ($r->total_cases - $r7->total_cases) / 7;
          $r->seven_day_cases = $r->total_cases - $r7->total_cases;
          $r->seven_day_cases_increase =  $r7->total_cases === 0 ?
            0 : ($r->total_cases - $r7->total_cases) * 100 / $r7->total_cases;
          if ($r7->total_cases == 0) {
            $r->doubling_cases = 9999999;
          } else if ((log($r->total_cases / $r7->total_cases)) == 0) {
            $r->doubling_cases = 9999999;
          } else
            $r->doubling_cases = (7 * log(2)) / (log($r->total_cases / $r7->total_cases));

          // Tests
          $r->seven_day_tests_average = ($r->total_tests - $r7->total_tests) / 7;
          $r->seven_day_tests = $r->total_tests - $r7->total_tests;
          $r->seven_day_tests_increase =  $r7->total_tests === 0 ?
            0 : ($r->total_tests - $r7->total_tests) * 100 / $r7->total_tests;
          if ($r7->total_tests == 0) {
            $r->doubling_tests = 9999999;
          } else if ((log($r->total_tests / $r7->total_tests)) == 0) {
            $r->doubling_tests = 9999999;
          } else
            $r->doubling_tests = (7 * log(2)) / (log($r->total_tests / $r7->total_tests));

          $r->save();
        }
      }
    });

    USHistoricalData::chunk(200, function ($records) use ($job) {
      foreach ($records as $r) {

        $carbonDate = Carbon::createFromFormat('Y-m-d', $r->day);
        if ($carbonDate->lessThan($job['earliestDate'])) {
          continue;
        }
        if ($carbonDate->greaterThan($job['latestDate'])) {
          continue;
        }

        $weekAgo = Misc::getWeekAgo($r->day);
        $r7 = USHistoricalData::where('day', $weekAgo)->first();

        if ($r7) {

          // Deaths
          $r->seven_day_deaths_average = ($r->total_deaths - $r7->total_deaths) / 7;
          $r->seven_day_deaths = $r->total_deaths - $r7->total_deaths;
          $r->seven_day_deaths_increase =  $r7->total_deaths === 0 ?
            0 : ($r->total_deaths - $r7->total_deaths) * 100 / $r7->total_deaths;
          if ($r7->total_deaths == 0) {
            $r->doubling_deaths = 9999999;
          } else if ((log($r->total_deaths / $r7->total_deaths)) == 0) {
            $r->doubling_deaths = 9999999;
          } else
            $r->doubling_deaths = (7 * log(2)) / (log($r->total_deaths / $r7->total_deaths));

          // Cases
          $r->seven_day_cases_average = ($r->total_cases - $r7->total_cases) / 7;
          $r->seven_day_cases = $r->total_cases - $r7->total_cases;
          $r->seven_day_cases_increase =  $r7->total_cases === 0 ?
            0 : ($r->total_cases - $r7->total_cases) * 100 / $r7->total_cases;
          if ($r7->total_cases == 0) {
            $r->doubling_cases = 9999999;
          } else if ((log($r->total_cases / $r7->total_cases)) == 0) {
            $r->doubling_cases = 9999999;
          } else
            $r->doubling_cases = (7 * log(2)) / (log($r->total_cases / $r7->total_cases));


          // Tests
          $r->seven_day_tests_average = ($r->total_tests - $r7->total_tests) / 7;
          $r->seven_day_tests = $r->total_tests - $r7->total_tests;
          $r->seven_day_tests_increase =  $r7->total_tests === 0 ?
            0 : ($r->total_tests - $r7->total_tests) * 100 / $r7->total_tests;
          if ($r7->total_tests == 0) {
            $r->doubling_tests = 9999999;
          } else if ((log($r->total_tests / $r7->total_tests)) == 0) {
            $r->doubling_tests = 9999999;
          } else
            $r->doubling_tests = (7 * log(2)) / (log($r->total_tests / $r7->total_tests));

          $r->save();
        }
      }
    });

    Misc::jobEnd('UpdateAveragesJob');
  }
}
