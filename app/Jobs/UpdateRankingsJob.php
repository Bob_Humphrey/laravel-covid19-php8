<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\StateHistoricalData;
use App\Helpers\Misc;
use Carbon\Carbon;

class UpdateRankingsJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  protected $field;
  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct($field)
  {
    $this->field = $field;
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('UpdateRankingsJob-' . $this->field);

    $rankField = "rank_" . $this->field;
    $date = $job['latestDate'];
    $endDate = $job['earliestDate'];
    $finalEndDate = Carbon::createFromFormat('Y-m-d', '2020-03-17');
    if ($endDate->lessThan($finalEndDate)) {
      $endDate = $finalEndDate;
    }

    $sortDirection = 'ASC';
    if (in_array(
      $this->field,
      ['doubling_deaths', 'doubling_cases', 'doubling_tests']
    )) {
      $sortDirection = 'DESC';
    }

    while ($date >= $endDate) {
      $date = $date->subDays(1);
      $formattedDate = $date->format('Y-m-d');
      $records = StateHistoricalData::where('day', $formattedDate)
        ->orderBy($this->field, $sortDirection)
        ->orderBy('state', 'DESC')
        ->get();
      $prevMeasure = '';
      $rank = 56;
      $tieRank = 56;
      foreach ($records as $record) {
        $measure = $record[$this->field];
        if ($measure === $prevMeasure) {
          $record[$rankField] = $tieRank;
          $rank--;
        } else {
          $record[$rankField] = $rank;
          $tieRank = $rank;
          $rank--;
          $prevMeasure = $measure;
        }
        $record->save();
      }
    }

    Misc::jobEnd('UpdateRankingsJob-' . $this->field);
  }
}
