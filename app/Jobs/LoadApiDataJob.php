<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Helpers\Misc;

class LoadApiDataJob implements ShouldQueue
{
  use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

  /**
   * Create a new job instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Execute the job.
   *
   * @return void
   */
  public function handle()
  {
    $job = Misc::jobStart('LoadApiDataJob');

    // Retrieve state data from dev api
    $query_string = 'https://covidtracking.com/api/states/daily';
    $response = Http::get($query_string)->getBody();
    $apiRecords = json_decode((string) $response);
    $count = count($apiRecords);

    for ($i = 0; $i < $count; $i++) {
      $r = $apiRecords[$i];
      $date = Misc::formatDate($r->date);
      if ($date['carbonDate']->lessThan($job['earliestDate'])) {
        continue;
      }
      if ($date['carbonDate']->greaterThan($job['latestDate'])) {
        continue;
      }

      $death = property_exists($r, 'death')  ? $r->death : 0;
      $death = $death === NULL ? 0 : $death;
      $deathIncrease = property_exists($r, 'deathIncrease') ? $r->deathIncrease : 0;
      $deathIncrease = $deathIncrease === NULL ? 0 : $deathIncrease;
      $totalTestResults = property_exists($r, 'totalTestResults') ? $r->totalTestResults : 0;
      $totalTestResults = $totalTestResults === NULL ? 0 : $totalTestResults;
      $totalTestResultsIncrease = property_exists($r, 'totalTestResultsIncrease') ? $r->totalTestResultsIncrease : 0;
      $totalTestResultsIncrease = $totalTestResultsIncrease === NULL ? 0 : $totalTestResultsIncrease;
      $positive = property_exists($r, 'positive') ? $r->positive : 0;
      $positive = $positive === NULL ? 0 : $positive;
      $positiveIncrease = property_exists($r, 'positiveIncrease') ? $r->positiveIncrease : 0;
      $positiveIncrease = $positiveIncrease === NULL ? 0 : $positiveIncrease;

      DB::table('state_historical_data')
        ->updateOrInsert(
          ['day' => $date['formattedDate'], 'state' => $r->state],
          [
            'total_deaths' => $death, 'daily_deaths' => $deathIncrease,
            'total_tests' => $totalTestResults, 'daily_tests' => $totalTestResultsIncrease,
            'total_cases' => $positive, 'daily_cases' => $positiveIncrease
          ]
        );
    }

    // Retrieve US data from dev api
    $query_string = 'https://covidtracking.com/api/us/daily';
    $response = Http::get($query_string)->getBody();
    $apiRecords = json_decode((string) $response);
    $count = count($apiRecords);

    for ($i = 0; $i < $count; $i++) {
      $r = $apiRecords[$i];
      $date = Misc::formatDate($r->date);
      if ($date['carbonDate']->lessThan($job['earliestDate'])) {
        continue;
      }
      if ($date['carbonDate']->greaterThan($job['latestDate'])) {
        continue;
      }

      $death = property_exists($r, 'death')  ? $r->death : 0;
      $death = $death === NULL ? 0 : $death;
      $deathIncrease = property_exists($r, 'deathIncrease') ? $r->deathIncrease : 0;
      $deathIncrease = $deathIncrease === NULL ? 0 : $deathIncrease;
      $totalTestResults = property_exists($r, 'totalTestResults') ? $r->totalTestResults : 0;
      $totalTestResults = $totalTestResults === NULL ? 0 : $totalTestResults;
      $totalTestResultsIncrease = property_exists($r, 'totalTestResultsIncrease') ? $r->totalTestResultsIncrease : 0;
      $totalTestResultsIncrease = $totalTestResultsIncrease === NULL ? 0 : $totalTestResultsIncrease;
      $positive = property_exists($r, 'positive') ? $r->positive : 0;
      $positive = $positive === NULL ? 0 : $positive;
      $positiveIncrease = property_exists($r, 'positiveIncrease') ? $r->positiveIncrease : 0;
      $positiveIncrease = $positiveIncrease === NULL ? 0 : $positiveIncrease;

      DB::table('us_historical_data')
        ->updateOrInsert(
          ['day' => $date['formattedDate'], 'state' => 'US'],
          [
            'total_deaths' => $death, 'daily_deaths' => $deathIncrease,
            'total_tests' => $totalTestResults, 'daily_tests' => $totalTestResultsIncrease,
            'total_cases' => $positive, 'daily_cases' => $positiveIncrease
          ]
        );
    }

    Misc::jobEnd('LoadApiDataJob');
  }
}
