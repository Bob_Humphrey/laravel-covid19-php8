@extends('layouts.app')

@section('content')

<div class="">
  <h2 class="font-inter_bold text-3xl text-gray-800">
    Definitions
  </h2>

  <dl class="pt-8 lg:w-4/5 text-justify">
    @foreach ($measures as $key => $value)
    <dt class="font-inter_medium text-lg text-gray-800">{{ $value }}</dt>
    <dd class="font-inter_regular text-base text-gray-600 pb-4">
      {{ $definitions[$key] }}
    </dd>
    @endforeach
  </dl>

  @endsection