<div class="text-base md:text-xl font-inter_semibold text-gray-600 border-b border-gray-400 pt-8">
  <table class="w-full">
    @foreach ($dataTable as $row)
      <tr class="grid grid-cols-12 py-1 border-t border-gray-400">
        <td class="col-span-7 py-2 pl-1 tooltip">
          <a class="hover:text-red-600" href={{ $row->url }}>
            <span class="tooltip-text">
              {{ $measureName }} - {{ $row->formattedDate }}
            </span>
            {{ $row->formattedDate }} </a>
        </td>
        <td class="col-span-4 py-2 px-2 text-right">
          {{ $row->formattedMeasure }}
        </td>
        <td
          class="flex justify-end self-center col-span-1 hover:text-red-600 hover:cursor-pointer py-2 text-right tooltip">
          <a href={{ $row->usUrl }}>
            <div class="w-6">
              <x-zondicon-shield />
            </div>
            <span class="tooltip-text">
              United States - {{ $row->formattedDate }}
            </span>
          </a>
        </td>
      </tr>
    @endforeach
  </table>
</div>
