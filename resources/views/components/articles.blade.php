<div class="text-lg font-inter_medium text-gray-600 pb-4">

  <h3 class="text-gray-800 text-2xl font-inter_bold">More Information</h3>

  <ul class="pb-4">
    <li class="hover:text-red-600">
      <a href="https://www.nytimes.com/interactive/2020/05/24/us/us-coronavirus-deaths-100000.html" target="_blank"
        rel="noopener noreferrer">
        An Incalculable Loss
      </a>
    </li>
    <li class="hover:text-red-600">
      <a href="https://www.theatlantic.com/health/archive/2020/05/patchwork-pandemic-states-reopening-inequalities/611866/"
        target="_blank" rel="noopener noreferrer">
        America’s Patchwork Pandemic Is Fraying Even Further
      </a>
    </li>
    <li class="hover:text-red-600">
      <a href="https://www.theatlantic.com/ideas/archive/2020/06/virus-will-win/612946/" target="_blank"
        rel="noopener noreferrer">
        The Virus Will Win
      </a>
    </li>
    <li class="hover:text-red-600">
      <a href="https://www.nytimes.com/interactive/2020/us/coronavirus-spread.html" target="_blank"
        rel="noopener noreferrer">
        How The Virus Won
      </a>
    </li>
    <li class="hover:text-red-600">
      <a href="https://www.theguardian.com/commentisfree/2020/jul/06/coronavirus-covid-19-mild-symptoms-who"
        target="_blank" rel="noopener noreferrer">
        Think a 'mild' case of Covid-19 doesn’t sound so bad? Think again
      </a>
    </li>
  </ul>

</div>