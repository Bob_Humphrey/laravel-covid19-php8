<div>
  <div class="text-5xl font-inter_black hover:text-red-600">
    <a href={{ url('/') }}>
      COVID 19
    </a>
  </div>
  <div class="font-inter_light mt-2 mb-4">
    April 1, 2020 through March 7, 2021
  </div>
</div>
