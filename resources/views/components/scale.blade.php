@php
use App\Helpers\Scales;
$counter = 0;
$scales = new Scales();
@endphp

<div class="grid grid-cols-9 text-xs md:text-sm font-inter_light pb-4 pt-6">
  @foreach ($scale as $item)
  @php
  $color = $scales->getColor($counter);
  $style = "background-color:" . $color . ";";
  $counter++;
  @endphp
  <div class="grid grid-cols-6 md:grid-cols-3">
    <div style={{ $style }}></div>
    <div class="col-span-5 md:col-span-2 md:py-2 pl-1">
      {{ $item }}
    </div>
  </div>
  @endforeach
</div>