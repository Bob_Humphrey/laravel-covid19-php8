<div class="text-gray-600 pt-12 pb-10">

  <div class="grid grid-cols-12 font-inter_extrabold text-2xl border-b border-gray-400 pb-2 mb-2">
    <div class="col-span-4">
    </div>
    <div class="col-span-4 text-right">
      Total
    </div>
    <div class="col-span-4 text-right">
      Daily
    </div>
  </div>

  <div class="grid grid-cols-12 font-inter_extrabold text-2xl border-b border-gray-400 pb-2 mb-2">
    <div class="col-span-4">
      Deaths
    </div>
    <div class="col-span-4 text-right">
      {{ number_format($usData->total_deaths) }}
    </div>
    <div class="col-span-4 text-right">
      {{ number_format($usData->daily_deaths) }}
    </div>
  </div>

  <div class="grid grid-cols-12 font-inter_extrabold text-2xl">
    <div class="col-span-4">
      Cases
    </div>
    <div class="col-span-4 text-right">
      {{ number_format($usData->total_cases) }}
    </div>
    <div class="col-span-4 text-right">
      {{ number_format($usData->daily_cases) }}
    </div>
  </div>

</div>
