<div class="text-2xl font-inter_semibold text-gray-600 pt-8">

  {!! $chart->container() !!}
  <script src="{{ $chart->cdn() }}"></script>
  {{ $chart->script() }}

</div>