@php
$showDefinitions = 'hidden';
if ($pageType === 'history' || 'day') {
$showDefinitions = '';
}
@endphp


<div class="font-inter_bold border-b border-gray-400 pb-4 mb-6" x-data="{ showDefinition: false }">
  <div class="flex">
    <h2 class="text-3xl leading-none text-gray-800 pb-2">
      {{ $pageTitle }}
    </h2>
    <div class={{ $showDefinitions }}>
      <div class="flex">
        <div class="text-gray-600 hover:text-red-600
        cursor-pointer w-7 ml-2" x-show="!showDefinition" @click="showDefinition=true">
          <x-zondicon-information-outline />
        </div>
        <div class="text-gray-600 hover:text-red-600 cursor-pointer w-7 ml-2" x-show="showDefinition"
          @click="showDefinition=false">
          <x-zondicon-close-outline />
        </div>
      </div>
    </div>
  </div>
  <div class="text-base font-inter_regular text-gray-600 py-4" x-show="showDefinition">
    {{ $definition }}
  </div>
  <div class="text-2xl  text-gray-600">
    {{ $pageSubtitle }}
  </div>
</div>
