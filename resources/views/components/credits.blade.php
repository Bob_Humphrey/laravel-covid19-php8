<div class="text-base font-inter_regular pb-4">
  <h3 class="text-gray-800 text-2xl font-inter_bold">Data</h3>
  <div class="text-gray-600">Data source for this dashboard is
    <a class="underline" href="https://covidtracking.com/" target="_blank" rel="noopener noreferrer">
      {{ ' ' }}The COVID Tracking Project
    </a>.
    NOTE: The COVID Tracking Project stopped compiling new data on March 7, 2021.
  </div>
</div>
