@extends('layouts.app')

@section('content')

<x-historyPageTitle :pageTitle="$pageTitle" stateName="United States" :measure="$measure" pageType="history" />
<x-historyDataChart :data="$data" :measure="$measure" />
<x-usHistoryDataTable :data="$data" :measure="$measure" :measureName="$pageTitle" />

@endsection