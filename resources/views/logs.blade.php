<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ config('app.name', 'Laravel') }}</title>

  <!-- Styles -->
  <link rel="stylesheet" href="{{ asset('css/all.css') }}">
  <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />

</head>

<body>

  <div class="text-base font-inter_regular text-gray-600 pt-8 px-20">
    <div class="mb-8">
      <a href={{ url('logs') }} class="text-black hover:text-red-600 mr-8">Summary</a>
      <a href={{ url('logs_detail') }} class="text-black hover:text-red-600">Detail</a>
    </div>
    <table class="w-full border-b border-gray-400">
      @foreach ($data as $row)
      @php
      $textColor = $row->job_name === "Director" && $report === 'detail' ? "text-red-600 grid grid-cols-12 py-1 border-t
      border-gray-400" : "grid grid-cols-12 py-1 border-t border-gray-400";
      @endphp
      <tr class="{{ $textColor }}">
        <td class="col-span-4 py-2 px-3">{{ $row->job_name }}</td>
        <td class="col-span-1 py-2 px-3">{{ $row->status }}</td>
        <td class="col-span-2 py-2 px-3">{{ $row->earliest_date }}</td>
        <td class="col-span-2 py-2 px-3">{{ $row->latest_date }}</td>
        <td class="col-span-2 py-2 px-3">{{ $row->created_at }}</td>
        <td class="col-span-1 text-right py-2 px-3">{{ $row->run_minutes }}</td>
      </tr>
      @endforeach
    </table>
  </div>

</body>

</html>