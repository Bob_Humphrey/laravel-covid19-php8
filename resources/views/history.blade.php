@php
// use Illuminate\Support\Facades\Log;
// Log::info("history.blade.php=$stateName")
@endphp


@extends('layouts.app')

@section('content')

<x-historyPageTitle :pageTitle="$pageTitle" :stateName="$stateName" :measure="$measure" pageType="history" />
<x-historyDataChart :data="$data" :measure="$measure" />
<x-scaleDisplay :scale="$scale" />
<x-historyDataTable :data="$data" :measure="$measure" :state="$state" :stateName="$stateName" />

@endsection