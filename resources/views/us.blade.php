@extends('layouts.app')

@section('content')

  <x-simplePageTitle :pageTitle="$pageTitle" :date="$date" />
  <x-dateMenu :path="$path" />
  <x-historyDataChart :data="$data" :date="$date" />
  <div class="text-xl text-center font-inter_semibold text-gray-600">Seven Day Cases Daily Average</div>
  <x-usDataTable :record="$record" :date="$date" :population="$population" />

@endsection
